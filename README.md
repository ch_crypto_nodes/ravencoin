# Ravencoin

A Ravencoin docker image.

## Tags

- [4.3.2.1](https://github.com/RavenProject/Ravencoin/releases/tag/v4.3.2.1)
- [4.1.0](https://github.com/RavenProject/Ravencoin/releases/tag/v4.1.0)
- [4.0.0](https://github.com/RavenProject/Ravencoin/releases/tag/v4.0.0)

*NOTE*:  
- [releases](https://github.com/RavenProject/Ravencoin/releases)  
- [ubuntu build](https://github.com/RavenProject/Ravencoin/blob/master/doc/build-ubuntu.md)

## What is Ravencoin?

Ravencoin is an experimental digital currency that enables instant payments to
anyone, anywhere in the world. Ravencoin uses peer-to-peer technology to operate
with no central authority: managing transactions and issuing money are carried
out collectively by the network. Raven Core is the name of open source
software which enables the use of this currency.

For more information, as well as an immediately useable, binary version of
the Raven Core software, see https://ravencoin.org

## Usage

### How to use this image

You must run a container with predefined environment variables.

Create `.env` file with following variables:

| Env variable                    | Description                                                                |
| ------------------------------- | -------------------------------------------------------------------------- |
| `RPC_USER_NAME`                 | User to secure the JSON-RPC api.                                           |
| `RPC_USER_PASSWORD`             | A password to secure the JSON-RPC api.                                     | 
| `PATH_TO_DATA_FOLDER`           | Full path to the blockchain data folder.                                   |

Run container: 

```bash
docker-compose up -d 

```

Stop container: 

```bash
docker-compose down 

```
